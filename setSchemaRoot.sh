#!/bin/bash
declare -A app
app[elg]="ELG-SHARE"
app[clarin]="CLARIN-SHARE"
app[elg-dev]="ELG-SHARE"
app[clarin-dev]="CLARIN-SHARE"

if [ "$1" = 'main' ]
  then
    echo "Setting schema root to META-SHARE.xsd"
  else
    echo "Setting schema root to ${app[$1]}.xsd"
    sed -i "s/META-SHARE/${app[$1]}/" /flask_app/flask_app/settings.py
fi