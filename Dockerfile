FROM python:3.8.1-alpine3.11

RUN apk update \
    && apk add --virtual build-deps build-base gcc musl-dev \
    && apk add --no-cache bash git openssh lighttpd openrc \
    && apk add libxml2-dev libxslt-dev

ARG app=main

COPY ./requirements.txt /requirements.txt

RUN pip install -r requirements.txt
WORKDIR /flask_app/static
# Force git clone in new image
ARG CACHE_DATE=2021-01-01
RUN git clone 'https://gitlab.com/ilsp-nlpli-metadata/metashare/metashare-schema.git' .
RUN git checkout $app
RUN git pull origin $app

WORKDIR /
ADD ./flask_app /flask_app
COPY ./deployment/lighttpd.conf /etc/lighttpd/lighttpd.conf
COPY ./deployment/deploy.sh /
RUN touch /flask_app/flask_app/log/api.log

COPY ./setSchemaRoot.sh /setSchemaRoot.sh
RUN chmod +x /setSchemaRoot.sh
RUN ./setSchemaRoot.sh $app

RUN chmod +x /deploy.sh
ENTRYPOINT /deploy.sh