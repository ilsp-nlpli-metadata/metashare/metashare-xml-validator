#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Using main branch"
    docker build -t registry.gitlab.com/ilsp-nlpli-metadata/metashare/metashare-xml-validator:latest .
  else
    echo "Using branch ${1}"
    docker build -t registry.gitlab.com/ilsp-nlpli-metadata/metashare/metashare-xml-validator:"${1}" --build-arg app="${1}" .
fi