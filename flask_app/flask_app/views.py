import zipfile
from flask_restful import Resource, reqparse, request

from xml_validator import VALIDATOR
from utils import validate_xml_file


class ValidateXML(Resource):
    def post(self):
        if not request.files.get('file'):
            return {'File missing error': 'No xml provided or zip file provided.'}, 400
        input_file = request.files['file']
        input_file_data = input_file.read()
        validator = VALIDATOR
        response = dict()
        # handle zip containing xml files
        if zipfile.is_zipfile(input_file):
            with zipfile.ZipFile(input_file, "r") as f:
                error_files = 0
                for xml_file in f.namelist():
                    errors = validate_xml_file(f.read(xml_file), xml_file, validator)
                    if not errors:
                        continue
                    else:
                        error_files += 1
                        response['info'] = f'{error_files} file(s) invalid'
                        response[xml_file] = errors
            if not error_files:
                response = dict(info=f'{len(f.namelist())} file(s) valid')
        # handle single file
        else:
            errors = validate_xml_file(input_file_data, input_file.filename, validator)
            if errors:
                response = dict(info=f"file '{str(input_file.filename)}' not valid",
                                errors=validate_xml_file(input_file_data, input_file.filename, validator))
            else:
                response = dict(info=f"file '{str(input_file.filename)}' valid")
        # Use JsonResponse instead of Response, just to prettify the response (e.g. in a curl command)
        return response, 200
