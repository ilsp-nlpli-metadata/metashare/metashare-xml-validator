from lxml import etree
from lxml.etree import XMLSyntaxError, DocumentInvalid


def validate_xml_file(input, filename, validator):
    """
    Validates a single xml file against settings.XSD_PATH schema
    """
    try:
        xml_doc = etree.fromstring(input)
    except XMLSyntaxError:
        error = list()
        error.append(dict(error=f"Could not parse file '{filename}': The file is not well formed or not an xml file"))
        return error
    try:
        return validator.xmlschema.assertValid(xml_doc)
    except DocumentInvalid as e:
        error_response = validator.parse_errors(e.error_log)
        return error_response
